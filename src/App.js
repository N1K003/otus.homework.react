import './App.css';
import {useRoutes} from './routes'
import {useAuth} from './hooks/auth.hook'
import {AuthContext} from './context/AuthContext'
import {BrowserRouter as Router} from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import rootReducer from './reducers/rootReducer'


function App() {
  const {token, login, logout} = useAuth()
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated)
  const store = createStore(rootReducer)

  return (
    <Provider store={store}>
      <AuthContext.Provider value={{
        token, login, logout, isAuthenticated
      }}>
        <Router>
          { isAuthenticated }
          <div className="container">
            {routes}
          </div>
        </Router>
      </AuthContext.Provider>
    </Provider>
  );
}

export default App;

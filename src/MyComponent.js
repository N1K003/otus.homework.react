import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import axios from 'axios';

export default class MyComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    getRandomQuoteId = () => {
        const maxQuoteId = 7;
        return Math.round(0.5 + Math.random() * maxQuoteId);
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (event) => {
        axios.get('/quotes/' + this.getRandomQuoteId())
        .then((response) =>
         {
            const { data: { quote = 'no quote found' } = {} } = response;
            this.setState({
                quoteText: quote
            })
         })
        .catch((error) => this.setState({
            quote: error.message
        }));
    }

    render() {
        return(
            <div className="container-content">
                <form noValidate autoComplete="off">
                    <TextField
                        className="container-text-field"
                        id="standard-multiline-flexible"
                        label="Random quote"
                        multiline
                        rowsMax={10}
                        value={!!this.state.quoteText ? this.state.quoteText : ''}
                        onChange={this.handleChange}
                    />

                    <Button
                        className="container-button"
                        variant="contained" 
                        color="primary" 
                        onClick={this.handleSubmit}>
                        Получить рандомную цитату
                    </Button>
                </form>
            </div>
        );
    }
}
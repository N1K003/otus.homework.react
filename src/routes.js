import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { Error404Page } from './pages/404Page'
import { HomePage } from './pages/HomePage'
import { LoginPage } from './pages/LoginPage'
import { RegisterPage } from './pages/RegisterPage'


export const useRoutes = isAuthenticated => {
    if (isAuthenticated) {
      return (
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route component={Error404Page} />
        </Switch>
      )
    }
  
    return (
      <Switch>
        <Route path={["/", "/login"]} exact component={LoginPage} />
        <Route path="/register" exact component={RegisterPage} />
        <Route component={Error404Page} />
      </Switch>
    )
}
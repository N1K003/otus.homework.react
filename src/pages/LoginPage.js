import '../App.css';
import React, {useContext, useState} from 'react'
import {AuthContext} from '../context/AuthContext'
import { useHistory } from 'react-router-dom';
import { useHttp } from "../hooks/http.hook";


export const LoginPage = () => {
    const auth = useContext(AuthContext)
    const {loading, request, error, clearError} = useHttp()
    const [form, setForm] = useState({
      email: '', password: ''
    })
    const history = useHistory();
    
    const changeHandler = event => {
        setForm({ ...form, [event.target.name]: event.target.value })
    }
    
    const routeOnChange = () => {
        const path = `/register`;
        history.push(path);
      }

    const loginHandler = async () => {
        try {
          //TODO - это для теста
          const data = {
            token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
            message: 'Демонстрационный вход',
          }
          auth.login(data.token)
          alert('login success')
        } catch (e) {}
      }
      
      return(
        <div className="row">
        <div className="col s6 offset-s3">
            <div className="card blue darken-1">
            <div className="card-content white-text">
                <span className="card-title">Авторизация</span>
                <div>
                <div className="input-field">
                    <input
                    placeholder="Введите email"
                    id="email"
                    type="text"
                    name="email"
                    className="yellow-input"
                    value={form.email}
                    onChange={changeHandler}
                    />
                    <label htmlFor="email">Email</label>
                </div>
                <div className="input-field">
                    <input
                    placeholder="Введите пароль"
                    id="password"
                    type="password"
                    name="password"
                    className="yellow-input"
                    value={form.password}
                    onChange={changeHandler}
                    />
                    <label htmlFor="password">Пароль</label>
                </div>
                </div>
            </div>
            <div className="card-action">
                <button
                className="btn yellow darken-4"
                style={{marginRight: 10}}
                disabled={loading}
                onClick={loginHandler}
                >
                Войти
                </button>
                <button
                className="btn grey lighten-1 black-text"
                disabled={loading}
                onClick={routeOnChange}
                >
                Регистрация
                </button>
            </div>
            </div>
        </div>
        </div>
    )
}
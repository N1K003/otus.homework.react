import '../App.css';
import React, {useEffect, useState} from 'react'
import { useHistory } from 'react-router-dom';
import { useHttp } from "../hooks/http.hook";

export const RegisterPage = () => {
    const {loading, request, error, clearError} = useHttp()
    const [form, setForm] = useState({
        email: '', password: ''
    })
    const history = useHistory();

    const changeHandler = event => {
        setForm({ ...form, [event.target.name]: event.target.value })
    }
    
    const registerHandler = async () => {
        alert('Not implemented')
      }
    
    const routeOnChange = () => {
        const path = `/login`;
        history.push(path);
      }
    return(
        <div className="row">
        <div className="col s6 offset-s3">
        <div className="card blue darken-1">
            <div className="card-content white-text">
            <span className="card-title">Авторизация</span>
            <div>
                <div className="input-field">
                <input
                    placeholder="Введите email"
                    id="email"
                    type="text"
                    name="email"
                    className="yellow-input"
                    value={form.email}
                    onChange={changeHandler}
                />
                <label htmlFor="email">Email</label>
                </div>
                <div className="input-field">
                <input
                    placeholder="Введите пароль"
                    id="password"
                    type="password"
                    name="password"
                    className="yellow-input"
                    value={form.password}
                    onChange={changeHandler}
                />
                <label htmlFor="password">Пароль</label>
                </div>
            </div>
            </div>
            <div className="card-action">
            <button
                className="btn yellow darken-4"
                style={{marginRight: 10}}
                disabled={loading}
                onClick={registerHandler}
            >
                Зарегистрироваться
            </button>
            <button
                className="btn grey lighten-1 black-text"
                disabled={loading}
                onClick={routeOnChange}
            >
            Логин
            </button>
            </div>
        </div>
        </div>
    </div>
    )
}